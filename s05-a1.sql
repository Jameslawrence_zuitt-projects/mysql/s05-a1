SELECT * FROM customers WHERE country="Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName="La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName="The Titanic";

SELECT firstName, lastName FROM employees WHERE email="jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName FROM employees WHERE lastName="Patterson" AND firstName="Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country <> "USA" AND creditLimit > 3000;

SELECT customerName FROM customers WHERE customerName NOT LIKE '%a%';

SELECT COUNT(orderNumber) FROM orders WHERE comments LIKE "%DHL%"; 

SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT country FROM customers GROUP BY country;

SELECT status FROM orders GROUP BY status;

SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");

SELECT employees.firstName, employees.lastName, offices.city FROM employees INNER JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";

SELECT customerName FROM customers WHERE salesRepEmployeeNumber IN (SELECT employeeNumber FROM employees WHERE firstName="Leslie" AND lastName="Thompson");

SELECT products.productName, customers.customerName FROM products INNER JOIN customers WHERE customers.customerName="Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;

SELECT lastName, firstName FROM employees WHERE reportsTo IN(SELECT employeeNumber FROM employees WHERE firstName="Anthony" AND lastName="Bow");

SELECT productName, MAX(MSRP)  FROM products; 

SELECT COUNT(*) FROM customers WHERE country="UK";

SELECT COUNT(*) FROM products WHERE productLine IN (SELECT productLine FROM productlines) GROUP BY productLine;

SELECT COUNT(*) FROM customers WHERE salesRepEmployeeNumber IN(SELECT employeeNumber FROM employees) GROUP BY salesRepEmployeeNumber;

SELECT productName, quantityInStock FROM products WHERE productLine="Planes" AND quantityInStock < 1000;


